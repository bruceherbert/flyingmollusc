extends Node2D

const Pickup = preload("res://src/objects/Coin.tscn")

onready var spawnPoints = $SpawnPoints
var items = 2

func _ready() -> void:
	spawn_pickup()

func get_spawn_position():
	var points = spawnPoints.get_children()
	points.shuffle()
	return points[0].global_position

func spawn_pickup():
	
	
	
	var spawn_position = get_spawn_position()
	var pickup = Pickup.instance()
	var main = get_node("../")
	spawnPoints.add_child(pickup)
	pickup.global_position = spawn_position
