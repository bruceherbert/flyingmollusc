extends Area2D

export var reduceVelocity:float
var minVelocity:float = 32

onready var anim_player: AnimationPlayer = get_node("AnimationPlayer")
onready var audio_player: AudioStreamPlayer = get_node("AudioStreamPlayer")
onready var value: Label = get_node("Label")

func _on_body_entered(body: Node) -> void:
	result()

func result() -> void:
	anim_player.play("collected")
	value.text = "SLOW"
	audio_player.play()
	if PlayerData.xVelocity >= minVelocity:
		PlayerData.xVelocity = PlayerData.xVelocity/reduceVelocity
