tool
extends Button

export(String, FILE) var next_scene_path: = ""
onready var scene_tree: = get_tree()

func _on_button_up() -> void:
	PlayerData.reset()
	load_scene()
	
func load_scene() -> void:
	get_tree().change_scene(next_scene_path)

func _get_configuration_warning() -> String:
	return "next_scene_path must be set for the button to work" if next_scene_path == "" else ""
