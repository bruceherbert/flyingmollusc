extends Node

var game_data

func _ready() -> void:
	var gamedata_file = File.new()
	gamedata_file.open("res://src/gamedata/LevelData.json", File.READ)
	var gamedata_json = JSON.parse(gamedata_file.get_as_text())
	gamedata_file.close()
	game_data = gamedata_json.result
