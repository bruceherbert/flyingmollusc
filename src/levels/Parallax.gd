extends ParallaxLayer

var cloud_speed = -PlayerData.xVelocity
export var depth: = 1

func _process(delta: float) -> void:
	self.motion_offset.x += cloud_speed * delta/depth
