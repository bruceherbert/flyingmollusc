extends Control


onready var scene_tree: = get_tree()
onready var score: Label = get_node("Label")

func _ready() -> void:
	PlayerData.connect("score_updated", self, "update_interface")
	PlayerData.connect("player_died", self, "_on_PlayerData_player_died")
	update_interface()

func update_interface() -> void:
	score.text = "%s" % PlayerData.score
