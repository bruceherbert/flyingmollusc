extends Node2D

var mapPieceWidth = 128
var maxVelocity:float = 128
var accelerationX:float = 1.0004
var currentLevel:int = 0
var levelIncrement:int = 1000
var currentPiece:String = ""

var loadedPool:Dictionary = {}

func _ready() -> void:
	randomize()
	create_next(pieceToLoad(), 0, 0)
	create_next(pieceToLoad(), mapPieceWidth, 0)
	
	
func _process(delta: float) -> void:
	updateDistance()
	updateVelocity()
	
func _on_mapPiece_piece_edge(xPos) -> void:
	var xPosOffset = xPos + mapPieceWidth
	create_next(pieceToLoad(), mapPieceWidth+xPosOffset, 0)

func pieceToLoad():
	# add more logic in here that uses the player score as a determining
	# factor for loading the scenes
	# randi - Generates a pseudo-random 32-bit unsigned integer between 0 and 4294967295 (inclusive)
	var data = ImportData.game_data.values()
	var level = getLevel(data)
	var chooseFrom = data[level].pool.trim_prefix("(").trim_suffix(")").replace(" ", "").split(",")
	var chooseFromSize = chooseFrom.size()
	
	var piece = "res://src/levels/mappieces/mapPiece" + str(chooseFrom[randi()%chooseFromSize]) + ".tscn"
	while piece == currentPiece:
		piece = "res://src/levels/mappieces/mapPiece" + str(chooseFrom[randi()%chooseFromSize]) + ".tscn"
		break
	currentPiece = piece
	return piece

# create next
func create_next(Piece, posX: float, posY: float) -> void:
	var scene = load(Piece).instance()
	add_child(scene)
	scene.connect("piece_edge", self, "_on_mapPiece_piece_edge")
	scene.position.x = posX

func updateDistance() -> void:
	PlayerData.score += ceil(PlayerData.xVelocity/128)
	
func updateVelocity() -> void:
	if PlayerData.xVelocity <= maxVelocity:
		PlayerData.xVelocity = PlayerData.xVelocity*accelerationX

func getLevel(data) -> int:
	# look at level# for current level - if greater, go to next level, increase 'current level'
	var poolSize = data.size()
	var getLevelData = data[currentLevel].scorerange.trim_prefix("(").trim_suffix(")").replace(" ", "")
	if PlayerData.score>int(getLevelData) && currentLevel<poolSize-1:
		currentLevel = currentLevel + 1
	return currentLevel
