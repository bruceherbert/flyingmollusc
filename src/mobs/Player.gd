extends KinematicBody2D

const FLOOR_NORMAL: = Vector2.UP
export var speed: = Vector2(0, 100.0)
export var gravity: = 400.0
var _velocity: = Vector2.ZERO
onready var touchingJump: = false
onready var animationPlayer = $AnimationPlayer

func _input(event):
	if event is InputEventScreenTouch and event.pressed:
		touchingJump = true
	else:
		touchingJump = false

func _physics_process(delta: float) -> void:
	# if jump is released AND we are moving - return true, else return false
	var is_jump_interrupted: = Input.is_action_just_released("jump") and _velocity.y < 0.0
	var direction: = get_direction()
	_velocity = calculate_move_velocity(_velocity, direction, speed, is_jump_interrupted)
	_velocity = move_and_slide(_velocity, FLOOR_NORMAL)
	var playBack = PlayerData.xVelocity/32
	animationPlayer.playback_speed = playBack
	if _velocity != Vector2(0,0):
		animationPlayer.play("jumping")
	else:
		animationPlayer.play("running")

func get_direction() -> Vector2:
	# if jump is pressed AND we are on floor - return 1, else return -1
	return Vector2(
		0,-1.0 if Input.is_action_just_pressed("jump") and is_on_floor() or touchingJump and is_on_floor() else 1.0
	)

func calculate_move_velocity(
		linear_velocity: Vector2,
		direction: Vector2,
		speed: Vector2,
		is_jump_interrupted: bool
	) -> Vector2:
	var out: = linear_velocity
	out.x = speed.x * direction.x
	out.y += gravity * get_physics_process_delta_time()
	if direction.y == -1.0:
		out.y = speed.y * direction.y
	if is_jump_interrupted:
		out.y = 0.0
	return out
