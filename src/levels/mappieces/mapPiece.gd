extends Node2D

signal piece_edge(xPos)


func _process(delta: float) -> void:
	self.position.x += -PlayerData.xVelocity * delta
	if self.position.x <= -128:
		destroy_self(self.position.x)

func destroy_self(xPos) -> void:
	emit_signal("piece_edge", xPos)
	queue_free()
