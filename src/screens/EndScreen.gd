extends Control


onready var labelFinal:Label = get_node("LabelFinal")
onready var labelBest:Label = get_node("LabelBest")

onready var transition_layer: CanvasLayer = $TransitionLayer
onready var anim_player: AnimationPlayer = $AnimationPlayer
onready var audio_player: AudioStreamPlayer = get_node("AudioStreamPlayer")

func _ready() -> void:
	if PlayerData.score > PlayerData.deaths:
		PlayerData.deaths = PlayerData.score

	labelFinal.text = labelFinal.text % [PlayerData.score]
	labelBest.text = labelBest.text % [PlayerData.deaths]


	anim_player.play("fade_in")
	audio_player.play()
	#needs to PAUSE THE GAME
	yield(anim_player, "animation_finished")
	transition_layer.queue_free()
