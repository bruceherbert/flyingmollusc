extends Node


signal score_updated
signal player_died
signal player_xVelocity


var score: float = 0 setget set_score
var deaths: = 0 setget set_deaths
var xVelocity: float = 64 setget set_xVelocity


func reset() -> void:
	score = 0
	xVelocity = 64
	

func set_score(value: float) -> void:
	score = value
	emit_signal("score_updated")
	
func set_deaths(value: int) -> void:
	deaths = value
	emit_signal("player_died")

func set_xVelocity(value: float) -> void:
	xVelocity = value
	emit_signal("player_xVelocity")
