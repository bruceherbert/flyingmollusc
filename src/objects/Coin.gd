extends Area2D


onready var anim_player: AnimationPlayer = get_node("AnimationPlayer")
onready var audio_player: AudioStreamPlayer = get_node("AudioStreamPlayer")
onready var value: Label = get_node("Label")
export var score: = 50

func _on_body_entered(body: Node) -> void:
	picked()
	
func picked() -> void:
	PlayerData.score += score
	value.text = "%s" % score
	anim_player.play("collected")
	audio_player.play()
